    <div class="social_network_icons">
        <a href="https://gnome.org/feed" target="_blank" aria-label="RSS Feed"><i class="fa fa-2x fa-rss" aria-hidden="true" title="RSS Feed"></i></a>
        <a href="https://www.facebook.com/GNOME" target="_blank" aria-label="GNOME on Facebook"><i class="fa fa-2x fa-facebook" aria-hidden="true" title="GNOME on Facebook"></i></a>
        <a href="https://twitter.com/gnome" target="_blank" aria-label="GNOME on Twitter"><i class="fa fa-2x fa-twitter" aria-hidden="true" title="GNOME on Twitter"></i></a>
    </div>
