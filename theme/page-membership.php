<?php

$members_url = "https://www.gnome.org/membership/memberslist";

$members = json_decode(file_get_contents($members_url), true);

?>

<?php get_header(); ?>

    <!-- container -->
    <div id="foundation" class="two_columns">
        <div class="container">
            <div class="content without_sidebar">
            <?php while ( have_posts() ) : the_post(); ?>
                <?php the_content(); ?>

                <?php

                if (isset($members)) {
                  echo '<ul class="foundation_members_list">'."\n";
                  foreach ($members['members'] as $member) {
                      echo '<li><a href="#" data-toggle="tooltip" title="Last renewed on: '. $member['last_renewed_on'] . '">'. $member['common_name'] . '</a></li>';
                  }
                  echo '</ul>'."\n";

                  echo '<h2>Emeritus Members</h2>';
                  echo '<hr style="margin:0; margin-bottom: 20px; padding:0;">';
                  echo '<ul class="foundation_members_list">'."\n";
                  foreach ($members['emeritus'] as $member) {
                      echo '<li><a href="#" data-toggle="tooltip" title="Last renewed on: '. $member['last_renewed_on'] . '">'. $member['common_name'] . '</a></li>';
                  }
                  echo '</ul>'."\n";
                } else {
                  echo '<p>Oops! The member list cannot be loaded at the moment.</p>';
                }

                ?>

            <?php endwhile; // End the loop. Whew. ?>
                <br />
                <div class="clear"></div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

<?php get_footer(); ?>
