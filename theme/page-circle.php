<?php

$apps_url = 'https://gitlab.gnome.org/Teams/Circle/-/raw/master/data/apps.json';
$apps = json_decode(file_get_contents($apps_url));

$libs_url = 'https://gitlab.gnome.org/Teams/Circle/-/raw/master/data/libs.json';
$libs = json_decode(file_get_contents($libs_url));

?>

<?php get_header(); ?>

    <!-- container -->
    <div class="container">
        <div class="content without_sidebar">
        <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>

            <?php

            if (isset($apps)) {
              echo '<h2>Circle Applications</h2>';
              echo '<ul class="circle_apps_list">'."\n";
              foreach ($apps as $app) {
                  echo '<li><a href="' . $app->url . '">' . $app->name . '</a></li>';
              }
              echo '</ul>'."\n";
            } else {
              echo '<p>Oops! The list cannot be loaded at the moment.</p>';
            }

            if (isset($libs)) {
              echo '<h2>Circle Libraries</h2>';
              echo '<ul class="circle_libs_list">'."\n";
              foreach ($libs as $lib) {
                  echo '<li><a href="' . $lib->url . '">' . $lib->name . '</a></li>';
              }
              echo '</ul>'."\n";
            } else {
              echo '<p>Oops! The list cannot be loaded at the moment.</p>';
            }

            ?>

        <?php endwhile; // End the loop. Whew. ?>
            <br />
            <div class="clear"></div>
        </div>
    </div>

    <div class="clearfix"></div>

<?php get_footer(); ?>
